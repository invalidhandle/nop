nop
========
	
Use a NOP object to write code that does nothing; this can be useful for running partially implemented code without commenting it out.  

The name for this package originates from the NOP opcode prevalent in assembly.
	



## Installation
```
pip install nop
```


## Usage
```python
from __future__ import print_function
from nop import NOP


n = NOP()

for i in n:
    print('this will not be shown')

with n('some', 'params') as something:
    print('this will be shown')

```


## License
[MIT](LICENSE.md)

